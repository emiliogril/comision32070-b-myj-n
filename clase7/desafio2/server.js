const express = require('express')

const app = express()	

app.get('/api/sumar/:num1/:num2', (req, res) => {
    const { num1, num2 } = req.params
    const result = parseInt(num1) + parseInt(num2)
    res.json({
        result
    })
})
app.get('/api/sumar', (req, res) => {
    const { num1, num2 } = req.query
    const result = parseInt(num1) + parseInt(num2)
    res.json({
        result
    })
})
app.get('/api/operaciones/:operac', (req, res) => {
    const { operac } = req.params
    const operacion = operac.split('+')
    const num1 = operacion[0]
    const num2 = operacion[1]
    const result = parseInt(num1) + parseInt(num2) 
    res.json({
        result
    })
})

const PORT = process.env.PORT || 4000
const server = app.listen(PORT, () => {
    console.log(`Server running on port ${server.address().port}`)
})