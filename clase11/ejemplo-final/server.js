const express = require('express')
const { Server: ServerHttp } = require('http')
const { Server: ServerIO } = require('socket.io')

const app = express()
const serverHttp = new ServerHttp(app)
const io = new ServerIO(serverHttp)

app.use(express.static('public'))

const mensajes = []

app.get('/', (req, res) => {
    res.sendFile('index.html', { root: __dirname })
})

io.on('connection', socket => {
    console.log('Nueva conexión')
    // socket.emit('mensaje-server', 'Bienvenido a la sala')
    
    socket.on('mj-cliente', data =>{
        console.log(data)
        mensajes.push({ socketId: socket.id, mensaje: data })
        
        io.sockets.emit('mensaje-server', mensajes)
        
    }) 
})

const server = serverHttp.listen(4000, () => {
    console.log(`Escuchando en el puerto ${server.address().port}`)
})