var getColor = function () { return Math.floor(Math.random() * 255); };
var Color = /** @class */ (function () {
    function Color() {
    }
    Color.prototype.get = function () {
        return "rgb(".concat(getColor(), ", ").concat(getColor(), ", ").concat(getColor(), ")");
    };
    return Color;
}());
var color = new Color();
console.log(color.get());
