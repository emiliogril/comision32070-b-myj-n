const express = require('express')
const  { Contenedor } =  require('./contenedor.js')
// import Contenedor from './contenedor.js'

const app = express()

/* Creating a route. productos */
app.get('/productos', async ( req, res )=> {
    try {
        const contenedor = new Contenedor('./productos.txt')        
        const productos = await contenedor.getAll()
        // console.log(productos)
        res.send({status: 200, productos})
        
    } catch (error) {
        res.send({status: 500, error})
    }
})

/* Creating a route.  productoRandom */
app.get('/productorandom', ( req, res )=> {
    const contenedor = new Contenedor('./productos.txt')
    // const producto = await contenedor.getProductRamdom()
    // res.send({status: 200, producto})
    
    contenedor.getProductRamdom()
        .then(producto => {
            res.send({status: 200, producto})
        })
        .catch(error => {
            res.send({status: 500, error})
        })
})



/* The above code is creating a server and listening to the port 4000. */
const PORT = process.env.PORT || 4000

const server = app.listen(PORT, ()=>{
    console.log(`Servidor corriendo en el puerto ${server.address().port}`)
})