const express = require('express')

const app = express()
const port = process.env.PORT || 4000

app.set('view engine', 'ejs')
app.set('views', './views')

app.get('/ejs', (req, res) => {
    let productos= [
        { nombre: 'Monitor', precio: '$200' },
        { nombre: 'Teclado', precio: '$50' },
        { nombre: 'Mouse', precio: '$10' }

    ] 
    let mensaje = {
        main:'Mensaje EJS',
        footer:'Footer EJS'
    }
    
    
    res.render('pages/index.ejs', {
        productos,
        mensaje
    })
})

app.get('/productos')

const server = app.listen(port, err => {
    if (err) throw new Error(`Error en el servidor: ${err}`)

    console.log(`Server running on port ${server.address().port}`)
})