const express = require('express')
const handlebars = require('express-handlebars')

const app = express()
const port = 4000 || process.env.PORT

app.engine(
    'hbs', 
    handlebars.engine({
        extname: '.hbs',
        defaultLayout: 'index.hbs',
        layoutsDir: __dirname + '/views',
        partialsDir: __dirname + '/views/partials'
    })
)

app.set('view engine', 'hbs')
app.set('views', './views')

app.use(express.static('public'))

const fakeApi = () => [
    {name: 'Fede', lane: 'midlaner'},
    {name: 'Fede1', lane: 'toplaner'},
    {name: 'Fede2', lane: 'midlaner'},
    {name: 'Fede3', lane: 'toplaner'},
    {name: 'Fede4', lane: 'midlaner'}
]

app.get('/', (req, res) => {
    res.render('index', {listExist: true, list: fakeApi() })
})



app.listen(4000, err => {
    if(err) throw new Error(`Erron on server: ${err}`)
    console.log(`Server is running on port ${port}`)
})




