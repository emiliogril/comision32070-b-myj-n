const express = require('express')
const multer = require('multer')

const app = express()

app.use( express.static(__dirname +'public') )

////////////////////////// config multer //////////////////////////

const storage = multer.diskStorage({
    destination: (req, _file, cb) => {
        cb(null, 'uploads')
    },
    filename: (req, file, cb) => {
        // console.dir(cb)
        cb(null, `${Date.now()}-${file.originalname}`)
    }
})

const upload = multer({ storage })

///////////////////////////// routes ///////////////////////////////

app.post('/uploadfile', upload.single('myFile'), (req, res, next) => {
    const { file } = req
    // console.log(file)
    // console.log(Date.now())
    if (!file) {
        const error =   new Error('Por favor suba un archivo')
        error.httpStatusCode = 400  
        return next(error)
    }
    res.send(file)
})
/// Muchos archivos
app.post('/uploadfiles', upload.array('myFiles'), (req, res, next)=>{
    const { files } = req
    // console.log(files)
    if (!files || files.length === 0) {
        const error =   new Error('Por favor suba un archivo como mínimo')
        error.httpStatusCode = 400  
        return next(error)
    }
    res.send(files)
} )



app.get('/api', (req, res) => {
    res.sendFile(__dirname + '/public/index.html')
})

app.listen(4000, () => {
    console.log('Server on port 3000')
})
