const express = require('express')
const { Server: HttpServer } = require('http')
const { Server: IOServer } = require('socket.io')  

const app = express()
const httpServer = new HttpServer(app)
const io = new IOServer(httpServer)

app.use(express.static('public'))

app.get('/', (req, res) => {
    res.sendFile('index.html', { root: __dirname })
})

const products = [
    { id: 1, name: 'Product 1', price: '$100' },
    { id: 2, name: 'Product 2', price: '$200' },
    { id: 3, name: 'Product 3', price: '$300' },
    { id: 4, name: 'Product 4', price: '$400' }   
]
io.on('connection', socket => {
    console.log('New user connected: ', socket.id)

    const message = {
        id: socket.id,
        message: 'Welcome to the app',
        products
    }

    socket.on('add-product', product => {
        products.push(product)
        io.sockets.emit('message-server', message)
    })
    
    socket.emit('message-server', message)

    socket.on('disconnect', () => {
        console.log('usuario desconectado: ', socket.id)
    })
})

const server = httpServer.listen(4000, () => {
    console.log(`Escuchando en el puerto ${server.address().port}`)
})
